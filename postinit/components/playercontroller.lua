local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local PlayerController = require("components/playercontroller")

local _DoControllerUseItemOnSceneFromInvTile = PlayerController.DoControllerUseItemOnSceneFromInvTile
function PlayerController:DoControllerUseItemOnSceneFromInvTile(item, ...)
    local is_equipped = item ~= nil and item:IsValid() and item.replica.equippable and item.replica.equippable:IsEquipped()
    if is_equipped then
        self.inst.replica.inventory:ControllerUseItemOnSceneFromInvTile(item)
    else
        _DoControllerUseItemOnSceneFromInvTile(self, item, ...)
    end
end

local _GetGroundUseAction = PlayerController.GetGroundUseAction
function PlayerController:GetGroundUseAction(position, ...)
    if self.inst:HasTag("_sailor") and self.inst:HasTag("sailing") then
        if position ~= nil then
            local landingPos = Vector3(TheWorld.Map:GetTileCenterPoint(position.x, 0, position.z))
            if landingPos.x == position.x and landingPos.z == position.z then
                local l = nil
                local r = BufferedAction(self.inst, nil, ACTIONS.DISEMBARK, nil, landingPos)
                return l, r
            end
        else
            --Check if the player is close to land and facing towards it
            local angle = self.inst.Transform:GetRotation() * DEGREES
            local dir = Vector3(math.cos(angle), 0, -math.sin(angle))
            dir = dir:GetNormalized()

            local myPos = self.inst:GetPosition()
            local step = 0.4
            local numSteps = 8
            local landingPos = nil

            for i = 0, numSteps, 1 do
                local testPos = myPos + dir * step * i
                local testTile = TheWorld.Map:GetTileAtPoint(testPos.x , testPos.y, testPos.z)
                if not IsWater(testTile) then
                    landingPos = testPos
                    break
                end
            end
            if landingPos then
                landingPos.x, landingPos.y, landingPos.z = TheWorld.Map:GetTileCenterPoint(landingPos.x, 0, landingPos.z)
                local l = nil
                local r = BufferedAction(self.inst, nil, ACTIONS.DISEMBARK, nil, landingPos)
                return l, r
            end
        end
    end
    return _GetGroundUseAction(self, position, ...)
end

local _GetPickupAction, _fn_i, scope_fn = UpvalueHacker.GetUpvalue(PlayerController.GetActionButtonAction, "GetPickupAction")
local function GetPickupAction(self, target, tool, ...)
    if not target:HasTag("smolder") and tool ~= nil then
        for k, v in pairs(TOOLACTIONS) do
            if target:HasTag(k.."_workable") then
                if tool:HasTag(k.."_tool") then
                    return ACTIONS[k]
                end
                -- break  Remove this break myself bc zarklord not respond to me _(:3」∠)_
            end
        end
    end
    local rets = {_GetPickupAction(self, target, tool)}
    if rets[1] == nil then
        if target.replica.inventoryitem ~= nil and
        target.replica.inventoryitem:CanBePickedUp() and
        not (target:HasTag("heavy") or (not target:HasTag("ignoreburning") and target:HasTag("fire")) or target:HasTag("catchable")) then
            rets[1] = (self:HasItemSlots() or target.replica.equippable ~= nil) and ACTIONS.PICKUP or nil
        end
    end
    return unpack(rets)
end
debug.setupvalue(scope_fn, _fn_i, GetPickupAction)

if not TheNet:IsDedicated() then
    local _IsVisible = Entity.IsVisible
    local function IsVisibleNotLocalNOCLICKed(self)
        return not IsLocalNOCLICKed(self) and _IsVisible(self)
    end

    local _UpdateControllerTargets, _fn_i, scope_fn = UpvalueHacker.GetUpvalue(PlayerController.UpdateControllerTargets, "UpdateControllerInteractionTarget")
    debug.setupvalue(scope_fn, _fn_i, function(self, ...)
        Entity.IsVisible = IsVisibleNotLocalNOCLICKed
        _UpdateControllerTargets(self, ...)
        Entity.IsVisible = _IsVisible
    end)
    --YEAH YEAH, this isn't a player controller post init, but it fits the theme of preventing selection of a entity on the client. -Z
    local _GetEntitiesAtScreenPoint = Sim.GetEntitiesAtScreenPoint
    function Sim:GetEntitiesAtScreenPoint(...)
        local entlist = {}
        for i, ent in ipairs(_GetEntitiesAtScreenPoint(self, ...)) do
            if not IsLocalNOCLICKed(ent) then
                entlist[#entlist + 1] = ent
            end
        end
        return entlist
    end
end
