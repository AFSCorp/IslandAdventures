local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Explosive = require("components/explosive")

-- install extra function calls
function Explosive:SetOnIgniteFn(fn)
	self.onignitefn = fn
end

local _OnIgnite = Explosive.OnIgnite
function Explosive:OnIgnite(...)
	_OnIgnite(self, ...)
	if self.onignitefn then
		self.onignitefn(self.inst)
	end
end