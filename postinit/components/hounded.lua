local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local function GroundTestCroc(pt)
    return IsInIAClimate(pt)
end

local function NoHolesCroc(pt)
    return GroundTestCroc(pt) and not TheWorld.Map:IsPointNearHole(pt)
end

local function NoHolesHound(pt)
    return not GroundTestCroc(pt) and not TheWorld.Map:IsPointNearHole(pt)
end

IAENV.AddComponentPostInit("hounded", function(cmp)
    local SuperHoundWaves = false
    local _SummonSpawn = UpvalueHacker.GetUpvalue(cmp.SummonSpawn, "SummonSpawn")
    if not _SummonSpawn then -- Super Hound Waves, probably
    	_SummonSpawn = UpvalueHacker.GetUpvalue(cmp.SummonSpawn, "OriginalSummonSpawn")
    	if _SummonSpawn then
    		SuperHoundWaves = true
    	end
    end
    local _GetSpawnPoint = UpvalueHacker.GetUpvalue(_SummonSpawn, "GetSpawnPoint")
    local _GetSpawnPrefab = UpvalueHacker.GetUpvalue(_SummonSpawn, "GetSpawnPrefab")
    local _GetSpecialSpawnChance = UpvalueHacker.GetUpvalue(_GetSpawnPrefab, "GetSpecialSpawnChance")
    local SPAWN_DIST = UpvalueHacker.GetUpvalue(_GetSpawnPoint, "SPAWN_DIST") or 30

    local function GetSpawnPoint(pt)
        if not TheWorld.Map:IsAboveGroundAtPoint(pt:Get()) then
            pt = FindNearbyLand(pt, 1) or pt
        end
        local offset = FindWalkableOffset(pt, math.random() * 2 * PI, SPAWN_DIST, 12, true, true, NoHolesHound)
        if offset ~= nil then
            offset.x = offset.x + pt.x
            offset.z = offset.z + pt.z
    		print("FOUND HOUND SPAWNPOINT",TheWorld.Map:GetTileAtPoint(offset.x, 0, offset.z))
            return offset
        end
    end

    local function GetSpawnPointCroc(pt)
        if not TheWorld.Map:IsAboveGroundAtPoint(pt:Get()) then
            pt = FindNearbyLand(pt, 1) or pt
        end
        local offset = FindWalkableOffset(pt, math.random() * 2 * PI, SPAWN_DIST, 12, true, true, NoHolesCroc)
        if offset ~= nil then
            offset.x = offset.x + pt.x
            offset.z = offset.z + pt.z
            return offset
        end
    end

    local function GetSpecialCrocChance()
    	-- same as hound chance, except we undo the season modifier
        local chance = _GetSpecialSpawnChance()
        return TheWorld.state.issummer and chance * 2/3 or chance
    end

    local function SummonSpawn(pt, upgrade, overrideprefab)
    	if GroundTestCroc(pt) or overrideprefab then
    		local spawn_pt = GetSpawnPointCroc(pt)
    		if spawn_pt ~= nil then
    			local spawn = SpawnPrefab(
    				overrideprefab
    				or	math.random() < GetSpecialCrocChance()
    					and ((TheWorld.state.isspring and "watercrocodog")
    						or (TheWorld.state.issummer and "poisoncrocodog"))
    				or "crocodog"
    			)
    			if spawn ~= nil then
    				spawn.Physics:Teleport(spawn_pt:Get())
    				spawn:FacePoint(pt)
    				if spawn.components.spawnfader ~= nil then
    					spawn.components.spawnfader:FadeIn()
    				end
    				return spawn
    			end
    		end
    	else
    		return _SummonSpawn(pt, upgrade) -- regular spawn behaviour, except we still edit GetSpawnPoint
    	end
    end

    -- Note: always set the deepest value first, or else we'd need to upvalue our own modifications
    UpvalueHacker.SetUpvalue(_SummonSpawn, GetSpawnPoint, "GetSpawnPoint")
    if SuperHoundWaves then
    	UpvalueHacker.SetUpvalue(cmp.SummonSpawn, SummonSpawn, "OriginalSummonSpawn")
    else
    	UpvalueHacker.SetUpvalue(cmp.SummonSpawn, SummonSpawn, "SummonSpawn")
    end

    function cmp:SummonSpecialSpawn(pt, prefab, num)
    	if pt == nil or prefab == nil then return end
    	for i = 1, (num and math.max(num, 1) or 1), 1 do
    		SummonSpawn(pt, false, prefab)
    	end
    end
end)
