local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local function mermhouse_postinit(inst)

    inst:AddTag("mermhouse")

end

IAENV.AddPrefabPostInit("mermhouse", mermhouse_postinit)
