local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------

local function trygrowhackable(inst)
	if inst:IsInLimbo()
		or (inst.components.witherable ~= nil
			and inst.components.witherable:IsWithered()) then
		return
	end

	if inst.components.hackable ~= nil then
		if inst.components.hackable:CanBeHacked() and inst.components.hackable.caninteractwith then
			return
		end
		inst.components.hackable:FinishGrowing()
	end
end

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local CANT_TAGS_PREFAB = {
    book_gardening = { "pickable", "stump", "withered", "barren", "INLIMBO" },
    book_horticulture = { "pickable", "stump", "withered", "barren", "INLIMBO", "silviculture", "tree", "winter_tree" },
    book_silviculture = { "pickable", "stump", "withered", "barren", "INLIMBO" },

}
local ONEOF_TAGS_PREFAB = {
    book_silviculture = { "silviculture", "tree", "winter_tree" },
}

local function fn(inst)
    if TheWorld.ismastersim then
        local CANT_TAGS
        if CANT_TAGS_PREFAB[inst.prefab] then
            CANT_TAGS = CANT_TAGS_PREFAB[inst.prefab]
        end
        local ONEOF_TAGS
        if ONEOF_TAGS_PREFAB[inst.prefab] then
            ONEOF_TAGS = ONEOF_TAGS_PREFAB[inst.prefab]
        end
        local _onread = inst.components.book.onread
        function inst.components.book.onread(inst, reader, ...)
            local ret = _onread(inst, reader, ...)
            if ret then -- should another mod make this book fail, then play along
                local x, y, z = reader.Transform:GetWorldPosition()
                local range = 30
                local ents = TheSim:FindEntities(x, y, z, range, nil, CANT_TAGS, ONEOF_TAGS)
                if #ents > 0 then
                    trygrowhackable(table.remove(ents, math.random(#ents)))
                    if #ents > 0 then
                        local timevar = 1 - 1 / (#ents + 1)
                        for i, v in ipairs(ents) do
                            v:DoTaskInTime(timevar * math.random(), trygrowhackable)
                        end
                    end
                end
            end
            return ret
        end
    end
end

IAENV.AddPrefabPostInit("book_gardening", fn) --incase someone wants to use the old unused book for some reason??? -Half
IAENV.AddPrefabPostInit("book_horticulture", fn)
IAENV.AddPrefabPostInit("book_silviculture", fn)
