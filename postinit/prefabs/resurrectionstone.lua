local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------

local sw_loot =
{
    "rocks",
    "limestonenugget",
    "nightmarefuel",
}

IAENV.AddPrefabPostInit("resurrectionstone", function(inst)
    if TheWorld.ismastersim and inst.components.lootdropper~= nil and IsInIAClimate(inst) then
        inst.components.lootdropper:SetLoot(sw_loot)
    end
end)
