local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local function peruse_meteor(inst)
    inst.components.sanity:DoDelta(TUNING.SANITY_LARGE)
end

IAENV.AddPrefabPostInit("wurt", function(inst)

    if not TheWorld.ismastersim then
        return inst
    end 
    inst.components.health.cantdrown_penalty = true
    inst.components.locomotor:SetFasterOnGroundTile(GROUND.TIDALMARSH, true)
    inst.components.foodaffinity:AddPrefabAffinity  ("seaweed", 1.33)
    inst.peruse_meteor = peruse_meteor

end)

