local assets=
{
	Asset("ANIM", "anim/ink_projectile.zip"),
	Asset("ANIM", "anim/ink_puddle.zip"),
}

local function onthrown(inst, thrower, pt, time_to_target)
    inst.Physics:SetFriction(.2)

    -- local shadow = SpawnPrefab("warningshadow")
    -- shadow.Transform:SetPosition(pt:Get())
    -- shadow:shrink(time_to_target, 1.75, 0.5)

	inst.TrackHeight = inst:DoPeriodicTask(FRAMES, function()
		local pos = inst:GetPosition()

		if pos.y <= 1 then
		    local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, 1.5, nil, {"FX", "NOCLICK", "DECOR", "INLIMBO"})

		    for k,v in pairs(ents) do
	            if v.components.combat and v ~= inst and v.prefab ~= "kraken_tentacle" then
	                v.components.combat:GetAttacked(thrower, TUNING.QUACKEN_INK_DAMAGE)
	            end
		    end

			--if inst:GetIsOnWater() then
				local splash = SpawnPrefab("kraken_ink_splat")
				splash.Transform:SetPosition(pos.x, pos.y, pos.z)

				inst.SoundEmitter:PlaySound("ia/common/cannonball_impact")
				inst.SoundEmitter:PlaySound("ia/creatures/seacreature_movement/splash_large")

				local ink = SpawnPrefab("kraken_inkpatch")
				ink.Transform:SetPosition(pos.x, pos.y, pos.z)
			--end

			inst:Remove()
		end
	end)
end

local function onremove(inst)
	if inst.TrackHeight then
		inst.TrackHeight:Cancel()
		inst.TrackHeight = nil
	end
end

local function fn()
	local inst = CreateEntity()        --实体
	local trans = inst.entity:AddTransform()   --变化
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("ink")
	inst.AnimState:SetBuild("ink_projectile")
	inst.AnimState:PlayAnimation("fly_loop", true)

	MakeInventoryPhysics(inst)

	inst:AddTag("thrown")
	inst:AddTag("projectile")

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("throwable")
	inst.components.throwable.onthrown = onthrown
	inst.components.throwable.random_angle = 0
	inst.components.throwable.max_y = 100
	inst.components.throwable.yOffset = 7

	inst.OnRemoveEntity = onremove

	inst.persists = false

	return inst
end

local lerp_time = TUNING.QUACKEN_INK_LINGERTIME

local function ink_update(inst, dt)
	inst.ink_timer = inst.ink_timer - dt
	inst.ink_scale = Lerp(0, 1, inst.ink_timer / lerp_time)
	inst.Transform:SetScale(inst.ink_scale, inst.ink_scale, inst.ink_scale)

	if inst.ink_scale <= 0.33 then
		--inst.slowing_player = false
		inst:Remove()
		return
	end
--[[
	local dist = inst:GetDistanceSqToClosestPlayer()
	if not inst.slowing_player and dist <= inst.ink_scale  then
		inst.slowing_player = true
	elseif inst.slowing_player and dist > inst.ink_scale  then
		inst.slowing_player = false
	end
--]]
end

local function inkpatch_fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

    inst.AnimState:SetBuild("ink_puddle")
    inst.AnimState:SetBank("ink_puddle")
    inst.AnimState:PlayAnimation("idle", true)
	inst.AnimState:SetLayer(LAYER_BACKGROUND)
	inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetSortOrder(ANIM_SORT_ORDER_BELOW_GROUND.UNDERWATER)
	inst.AnimState:SetSortOrder(3)

	inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("unevenground") --Slow down, it's much better than the slow down of SW(减速，这比SW的减速要强很多) -k
	inst.ink_timer = lerp_time
	inst.ink_scale = 1
	inst.Transform:SetScale(inst.ink_scale, inst.ink_scale, inst.ink_scale)
	local dt = FRAMES * 3
	inst:DoPeriodicTask(dt, function() ink_update(inst, dt) end)
	return inst
end

return Prefab("kraken_projectile", fn, assets),
		Prefab("kraken_inkpatch", inkpatch_fn)